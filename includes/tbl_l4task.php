<?php

function tbl_l4task_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="l4task Management";
    $objType="l4task";
    $tblname="tbl_l4task";
    $idcol="l4taskid";
    require_once 'classes/class.tbl_l4task.php';
    $objl4task = new l4task();
    processl4task($objl4task,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listl4task':

                listl4task($objl4task,$tblname,$idcol,$menu);
                break;

            case 'addl4task':
                addl4task($objl4task,$tblname,$idcol,$menu);
                break;
            case 'editl4task':
                editl4task($objl4task,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listl4task($objl4task,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listl4task($objl4task,$tblname,$idcol,$menu);
    }

}
function processl4task($objl4task,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objl4task->delete($id)){
                    $objl4task->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['l4taskname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['l4taskname']);
        $email  = strip_tags($_POST['email']);

        try{
            if($id !== ""){

                if($objl4task->update($name, $email, $id)){

                    $objl4task->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objl4task->insert($name, $email)){
                    $objl4task->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objl4task->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listl4task($objl4task,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <?php
            $query = "SELECT * FROM $tblname";
            $stmt = $objl4task->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl4task = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl4task[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl4task&edit_id=<?php print($rowl4task[$idcol]);?>">
                                <?php print($rowl4task['l4taskname']); ?></a>
                        </td>

                        <td><?php print($rowl4task['email']);  ?></td>

                        <td>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowl4task[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function addl4task($objl4task,$tblname,$idcol,$menu)
{
    echo "test";
    l4taskform($objl4task,$tblname,$idcol,$menu);
}
function editl4task($objl4task,$tblname,$idcol,$menu)
{
    //echo "test";
    l4taskform($objl4task,$tblname,$idcol,$menu);
}

function l4taskform($objl4task,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objl4task->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowl4task = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowl4task = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowl4task[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="l4taskname" id="l4taskname" placeholder="First Name and Last Name" value="<?php print($rowl4task['l4taskname']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Email *</label>
            <input  class="form-control" type="text" name="email" id="email" placeholder="johndoel@gmail.com" value="<?php print($rowl4task['email']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}