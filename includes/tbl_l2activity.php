<?php

function tbl_l2activity_mgt($projid)
{
    $menu="";

    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="Activity Management";
    $objType="l2activity";
    $tblname="tbl_l2activity";
    $idcol="l2activityID";
    require_once 'classes/class.tbl_l2activity.php';
    $objl2activity = new l2activity();
    processl2activity($objl2activity,$menu);

    echo "<h2 style=\"margin-top: 10px\">$subpagetitle</h2>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }

    if($menu=='activity')
    {

        if(isset($_REQUEST['submenu']))
        {
            $submenu=$_REQUEST['submenu'];

            switch ($submenu)
            {
                case 'listl2activity':
                    //echo "HAHAa";
                    listl2activity($projid,$objl2activity,$tblname,$idcol,$menu);
                    break;
                case 'viewl2activity':

                    viewl2activity($projid,$objl2activity,$tblname,$idcol,$menu);
                    break;
                case 'addproj':
                    addl2activity($projid,$objl2activity,$tblname,$idcol,$menu);
                    break;
                case 'editl2activity':
                    editl2activity($projid,$objl2activity,$tblname,$idcol,$menu);
                    break;
                default:
                    echo "HAHA";
                    listl2activity($projid,$objl2activity,$tblname,$idcol,$menu);

            }
        }
        else
        {
            listl2activity($projid,$objl2activity,$tblname,$idcol,$menu);
        }
    }
    else    //if this is a menu within project or else where
    {
        $menu="activity";
        listl2activity($projid,$objl2activity,$tblname,$idcol,$menu);
    }
}
function processl2activity($objl2activity,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objl2activity->delete($id)){
                    $objl2activity->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['l2activityname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $posteddata[]="";
        echo "<table>";
        foreach ($_POST as $key => $value) {
            $posteddata[$key]=$value;
            echo "<tr>";
            echo "<td>";
            echo $key;
            echo "</td>";
            echo "<td>";
            echo $posteddata[$key];
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";

        $accID= $posteddata['accID'];
        $projName= $posteddata['projName'];
        $projRef= $posteddata['projRef'];
        $projStatus= $posteddata['projStatus'];
        $projManager= $posteddata['projManager'];
        $projManagerTeam= $posteddata['projManagerTeam'];
        $projLocation= $posteddata['projLocation'];
        $projDetailLocation= $posteddata['projDetailLocation'];
        $projTargetStart= $posteddata['projTargetStart'];
        $projTargetEnd= $posteddata['projTargetEnd'];
        $projScheduleStart= $posteddata['projScheduleStart'];
        $projScheduleEnd= $posteddata['projScheduleEnd'];
        $projActEnd=$posteddata['projActEnd'];
        $projActStart=$posteddata['projActStart'];

        try{
            if($id !== ""){

                if($objl2activity->update($accID, $projName,$projRef,$projStatus,$projManager,$projManagerTeam,$projLocation,$projDetailLocation,$projTargetStart,$projTargetEnd,$projScheduleStart,$projScheduleEnd,$projActStart,$projActEnd, $id)){

                    $objl2activity->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objl2activity->insert($accID, $projName,$projRef,$projStatus,$projManager,$projManagerTeam,$projLocation,$projDetailLocation,$projTargetStart,$projTargetEnd,$projScheduleStart,$projScheduleEnd)){
                //    $objl2activity->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objl2activity->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listl2activity($projid,$objl2activity,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <?php
    $query = "SELECT * FROM $tblname WHERE `projID`=$projid;";
    $stmt = $objl2activity->runQuery($query);
    $stmt->execute();

    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>projID</th>
                <th>actRef</th>
                <th>Description</th>
                <th>Location L1</th>
                <th>Location L2</th>
                <th>Equipment</th>
                <th>actType</th>
                <th>Planner Group</th>
                <th>Work Center</th>
                <!--
                <th>Detail Location</th>
                <th>Target Start</th>
                <th>Target End</th>
                <th>Schedule Start</th>
                <th>Schedule End</th>
                <th>Actual Start</th>
                <th>Actual End</th>
                -->
                <th></th>
            </tr>
            </thead>

            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl2activity = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl2activity[$idcol]);    ?>
                        </td>

                        <td><?php print($rowl2activity['projID']);    ?></td>
                        <td><?php print($rowl2activity['activityReference']);  ?></td>
                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=viewl2activity&projid=<?php echo $projid;?>&actid=<?php print($rowl2activity[$idcol]);?>" target="activity">
                                <?php print($rowl2activity['Description']); ?></a>
                        </td>


                        <td><?php print($rowl2activity['actLocation']);  ?></td>
                        <td><?php print($rowl2activity['actLocation1']);  ?></td>
                        <td><?php print($rowl2activity['Equipment']);  ?></td>
                        <td><?php print($rowl2activity['actType']);  ?></td>
                        <td><?php print($rowl2activity['Planner Group']);  ?></td>
                        <td><?php print($rowl2activity['Work Center']);  ?></td>


                        <td>

                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl2activity&projid=<?php echo $projid;?>&edit_id=<?php print($rowl2activity[$idcol]);?>" target="activity">
                                <span data-feather="edit"></span></a>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&projid=<?php echo $projid;?>&delete_id=<?php echo $rowl2activity[$idcol]; ?>" target="activity">
                                <span data-feather="trash"></span>
                            </a>

                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function viewl2activity($projid,$objl2activity,$tblname,$idcol,$menu)
{
    $id=$_REQUEST['actid'];
    $query = "SELECT * FROM $tblname WHERE $idcol=$id";
    $stmt = $objl2activity->runQuery($query);
    $stmt->execute();

    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
              <th>projID</th>
                <th>actRef</th>
                <th>Description</th>
                <th>Location L1</th>
                <th>Location L2</th>
                <th>Equipment</th>
                <th>actType</th>
                <th>Planner Group</th>
                <th>Work Center</th>

                <th></th>
            </tr>
            </thead>

            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl2activity = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl2activity[$idcol]);    ?>
                        </td>

                        <td><?php print($rowl2activity['projID']);    ?></td>
                        <td><?php print($rowl2activity['activityReference']);  ?></td>
                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=viewl2activity&projid=<?php echo $projid;?>&actid=<?php print($rowl2activity[$idcol]);?>" target="activity">
                                <?php print($rowl2activity['Description']); ?></a>
                        </td>


                        <td><?php print($rowl2activity['actLocation']);  ?></td>
                        <td><?php print($rowl2activity['actLocation1']);  ?></td>
                        <td><?php print($rowl2activity['Equipment']);  ?></td>
                        <td><?php print($rowl2activity['actType']);  ?></td>
                        <td><?php print($rowl2activity['Planner Group']);  ?></td>
                        <td><?php print($rowl2activity['Work Center']);  ?></td>


                        <td>

                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl2activity&projid=<?php echo $projid;?>&edit_id=<?php print($rowl2activity[$idcol]);?>" target="activity">
                                <span data-feather="edit"></span></a>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&projid=<?php echo $projid;?>&delete_id=<?php echo $rowl2activity[$idcol]; ?>" target="activity">
                                <span data-feather="trash"></span>
                            </a>

                            </a>
                        </td>
                        <th></th>
                    </tr>


            </tbody>
        </table>
        <!-- ACTIVITY DETAILS  -->
        <h2>Activity Details</h2>

        <!-- SUBACTIVITY LISTINGs-->

    </div>
     <?php
                }
            }
            ?>
    <?php

}

function addl2activity($projid,$objl2activity,$tblname,$idcol,$menu)
{
    echo "test";
    l2activityform($objl2activity,$tblname,$idcol,$menu);
}
function editl2activity($projid,$objl2activity,$tblname,$idcol,$menu)
{
    //echo "test";
    l2activityform($projid,$objl2activity,$tblname,$idcol,$menu);
}

function l2activityform($projid,$objl2activity,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objl2activity->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowl2activity = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowl2activity = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowl2activity[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">AccID *</label>
            <input  class="form-control" type="text" name="accID" id="accID" placeholder="account ID" value="<?php print($rowl2activity['accID']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="name">Project Name *</label>
            <input  class="form-control" type="text" name="projName" id="projName" placeholder="Project Name" value="<?php print($rowl2activity['projName']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Ref *</label>
            <input  class="form-control" type="text" name="projRef" id="projRef" placeholder="Project Reference" value="<?php print($rowl2activity['projRef']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Status *</label>
            <input  class="form-control" type="text" name="projStatus" id="projStatus" placeholder="Project Status" value="<?php print($rowl2activity['projStatus']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Manager *</label>
            <input  class="form-control" type="text" name="projManager" id="projManager" placeholder="projManager" value="<?php print($rowl2activity['projManager']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Project Team *</label>
            <input  class="form-control" type="text" name="projManagerTeam" id="projManagerTeam" placeholder="projManagerTeam" value="<?php print($rowl2activity['projManagerTeam']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Location *</label>
            <input  class="form-control" type="text" name="projLocation" id="projLocation" placeholder="projLocation" value="<?php print($rowl2activity['projLocation']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Detail Location *</label>
            <input  class="form-control" type="text" name="projDetailLocation" id="projDetailLocation" placeholder="projDetailLocation" value="<?php print($rowl2activity['projDetailLocation']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Target Start *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl2activity['projTargetStart']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projTargetStart" id="projTargetStart" placeholder="projTargetStart" value="<?php print($ddate); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Target End *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl2activity['projTargetEnd']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projTargetEnd" id="projTargetEnd" placeholder="projTargetEnd" value="<?php print($ddate); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Schedule Start *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl2activity['projScheduleStart']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projScheduleStart" id="projScheduleStart" placeholder="projScheduleStart" value="<?php print($ddate); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Schedule End *</label>
            <?php $ddate=date("Y-m-d",strtotime($rowl2activity['projScheduleEnd']));
            if($ddate=='1970-01-01'){$ddate='';}
            ?>
            <input  class="form-control" type="date" name="projScheduleEnd" id="projScheduleEnd" placeholder="projScheduleEnd" value="<?php print($ddate); ?>" required maxlength="100">
        </div>

        <?php if(isset($_REQUEST['edit_id']))
            {?>
                <div class="form-group">
                    <label for="email">Act End *</label>
                    <?php $ddate=date("Y-m-d",strtotime($rowl2activity['projActStart']));
                    if($ddate=='1970-01-01'){$ddate='';}
                    ?>
                    <input  class="form-control" type="date" name="projActStart" id="projActStart" placeholder="projActStart" value="<?php print($ddate); ?>" required maxlength="100">
                </div>
                <div class="form-group">
                    <?php $ddate=date("Y-m-d",strtotime($rowl2activity['projActEnd']));
                    if($ddate=='1970-01-01'){$ddate='';}
                    ?>
                    <label for="email">Act End *</label>
                    <input  class="form-control" type="date" name="projActEnd" id="projActEnd" placeholder="projActEnd" value="<?php print($ddate); ?>" required maxlength="100">
                </div>

            <?php
            }
            ?>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}