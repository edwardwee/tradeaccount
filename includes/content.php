<?php


if(isset($_GET['menu'])){$menu=$_GET['menu'];}

if(isset($_GET['submenu'])){$submenu=$_GET['submenu'];}

if(isset($menu))
{

    switch($menu)
    {
        case 'user':
            include("includes/tbl_user.php");
            //echo $menu;
            tbl_user_mgt();
            break;
        case 'tradeacc':
            include("includes/tradeacc.php");
            tbl_account_mgt();
            break;

            /*
        case 'account':
            include("includes/tbl_account.php");
            tbl_account_mgt();
            break;
        case 'project':
            include("includes/tbl_l1proj.php");
            tbl_l1proj_mgt();
            break;
        case 'activity':
            include("includes/tbl_l2activity.php");
            $projid=$_REQUEST['projid'];
            tbl_l2activity_mgt($projid);
            break;
        case 'tasklist':
            include("includes/tbl_mtasklist.php");
            tbl_mtasklist_mgt();
            break;

            */
        case 'logout':
            //include("includes/tbl_mtasklist.php");
            logout();
            break;
        case 'sample':
            fromsample();
            break;
        case 'sampleform':
            sampleform();
            break;



    }
}

?>
<?php



function fromsample()
{

    require_once 'classes/user.php';

    $objUser = new User();

// GET
    if(isset($_GET['delete_id'])){
        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objUser->delete($id)){
                    $objUser->redirect('index.php?menu=sample&deleted');
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    echo "
    
        <h1 style=\"margin-top: 10px\">DataTable</h1>
    ";
    if(isset($_GET['updated'])){
        echo '<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>User!<trong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>';
    }else if(isset($_GET['deleted'])){
        echo '<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>User!<trong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>';
    }else if(isset($_GET['inserted'])){
        echo '<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>User!<trong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>';
    }else if(isset($_GET['error'])){
        echo '<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>DB Error!<trong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>';
    }
    echo "
    <div class=\"table-responsive\">
        <table class=\"table table-striped table-sm\">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            ";
            $query = "SELECT * FROM tbl_user";
            $stmt = $objUser->runQuery($query);
            $stmt->execute();
    echo "<tbody>";

             if($stmt->rowCount() > 0){
                while($rowUser = $stmt->fetch(PDO::FETCH_ASSOC)){
                    echo "
                    <tr>
                        <td>";
                            print($rowUser['userid']);
                   echo "</td>

                        <td>
                            <a href=\"index.php?menu=sampleform&edit_id=";
                                print($rowUser['userid']); echo "\">";
                                print($rowUser['username']);
                            echo "</a>
                        </td>

                        <td>";
                            print($rowUser['email']);

                            echo "</td>

                        <td>
                            <a class=\"confirmation\" href=\"index.php?menu=sample&delete_id=";
                            echo $rowUser['userid'];
                            echo "\">
                                <span data-feather=\"trash\"></span>
                            </a>
                        </td>
                    </tr>
                    ";


                }
             }
             echo "
            </tbody>
        </table>

    </div>

    ";

}

function sampleform()
{
    require_once 'classes/user.php';

    $objUser = new User();
// GET
    if(isset($_GET['edit_id'])){
        $userid = $_GET['edit_id'];
        $stmt = $objUser->runQuery("SELECT * FROM tbl_user WHERE userid=:userid");
        $stmt->execute(array(":userid" => $userid));
        $rowUser = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $userid = null;
        $rowUser = null;
    }

// POST
    if(isset($_POST['btn_save'])){
        $name   = strip_tags($_POST['username']);
        $email  = strip_tags($_POST['email']);

        try{
            if($userid != null){
                if($objUser->update($name, $email, $userid)){
                    $objUser->redirect('index.php?menu=sample&updated');
                }
            }else{
                if($objUser->insert($name, $email)){
                    $objUser->redirect('index.php?menu=sample&inserted');
                }else{
                    $objUser->redirect('index.php?menu=sample&error');
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    ?>
    <h1 style="margin-top: 10px">Add / Edit Users</h1>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="sample">
        <div class="form-group">
            <label for="id">ID</label>
            <input class="form-control" type="text" name="userid" id="userid" value="<?php print($rowUser['userid']); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="name" id="name" placeholder="First Name and Last Name" value="<?php print($rowUser['username']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Email *</label>
            <input  class="form-control" type="text" name="email" id="email" placeholder="johndoel@gmail.com" value="<?php print($rowUser['email']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

<?php
}

?>