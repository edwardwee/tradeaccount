


<?php

function tbl_account_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="account Management";
    $objType="account";
    $tblname="mytrades";
    $idcol="id";
    require_once 'classes/class.tradeacc.php';
    $objaccount = new account();
    processaccount($objaccount,$menu);

    echo "<h1 style=\"margin-top: 10px\">Account Management</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }

       // echo "hello";

    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listtradeacc':

                listtradeacc($objaccount,$tblname,$idcol,$menu);

                //listtradeacc1($objaccount,$tblname,$idcol,$menu);
                break;

            case 'addacc':
                echo "testacc entry";
                addaccount($objaccount,$tblname,$idcol,$menu);
                break;
            case 'editaccount':
                editaccount($objaccount,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listtradeacc($objaccount,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listtradeacc($objaccount,$tblname,$idcol,$menu);
    }

}
function processaccount($objaccount,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objaccount->delete($id)){
                    $objaccount->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['accountname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['accountname']);
        $company= strip_tags($_POST['company']);
        $owner= strip_tags($_POST['owner']);
        $admin= strip_tags($_POST['admin']);

        try{
            if($id !== ""){

                if($objaccount->update($name,$company,$owner,$admin,  $id)){

                    $objaccount->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objaccount->insert($name,$company,$owner,$admin)){
                    $objaccount->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objaccount->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listtradeacc($objaccount,$tblname,$idcol,$menu)
{
    //echo "test";
    //echo "Logged in As ".$_SESSION['username']."<p>";

    //Challenges
    echo "<h2> CHALLENGE ACCOUNTS</h2>";
    $uname = $_SESSION['username'];
    $query = "SELECT * FROM $tblname WHERE `status`<>'Completed' AND `targetbal`<>0 AND `username`='$uname' ORDER BY `manager`,(`targetbal`-`currentbal`),`phase`, `accountname`";
    $stmt = $objaccount->runQuery($query);
    $stmt->execute();

    ?>

    <?php
    $currmgr='';
    if($stmt->rowCount() > 0)
    {
        while($rowaccount = $stmt->fetch(PDO::FETCH_ASSOC)){
            //echo "<h2>Managed by $currmgr </h2>";
            if ($currmgr=='' || $currmgr!==$rowaccount['manager']) {



                if($currmgr!==$rowaccount['manager']&&$currmgr!=='')
                {
                    ?>
                    </tbody>
                    </table>
                    </div>

                    <?php
                }

                $currmgr = $rowaccount['manager'];
                ?>

                    <div class="table-responsive">
                        <?php  echo " Managed by : $currmgr "; ?>
                    <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>AccNo</th>
                        <th>AccName</th>

                        <th>Ph</th>
                        <th>Start Bal</th>
                        <th>Today Equity</th>
                        <th>Balance</th>
                        <th>Equity</th>
                        <th>P&L</th>
                        <th>Remaining Target</th>
                        <th>Days Left</th>
                        <th>Last updated</th>
                        <th>PropFirm</th>
                    </tr>
                    </thead>

                    <tbody>
                <?php
            }

            ?>




                    <tr>
                        <td>
                            <?php print($rowaccount[$idcol]);    ?>
                        </td>

                        <td>
                            <!--<a href="index.php?menu=<?php echo $menu;?>&submenu=editaccount&edit_id=<?php print($rowaccount[$idcol]);?>">
                                <?php print($rowaccount['accountno']); ?></a>-->
                            <?php print($rowaccount['accountno']); ?>
                        </td>

                        <td><?php print($rowaccount['accountname']);  ?></td>

                        <td><?php print($rowaccount['phase']);  ?></td>
                        <td><?php print(number_format($rowaccount['startbal'],2,'.',','));  ?></td>
                        <td><?php print(number_format($rowaccount['startdayequity'],2,'.',','));  ?></td>

                        <td><?php print(number_format($rowaccount['currentbal'],2,'.',','));  ?></td>
                        <td><?php print(number_format($rowaccount['equity'],2,'.',','));  ?></td>
                        <td>
                            <?php
                            $pnl=$rowaccount['equity']-$rowaccount['startdayequity'];
                            print(number_format($pnl,2,'.',','));
                            ?>
                        </td>


                        <td>
                            <?php
                            if($rowaccount['phase']<3)
                            {
                                $remainbal=$rowaccount['targetbal']-$rowaccount['currentbal'];
                                print(number_format($remainbal,2,'.',','));
                            }
                            ?>
                        </td>
                        <td>
                            <?php

                                //$remainbal=$rowaccount['startdate']-$rowaccount['currentbal'];
                                $date1=date_create(date("Y-m-d"));
                                $date2=date_create($rowaccount['enddate']);
                                $diff=date_diff($date1,$date2);
                                echo $diff->format("%R%a days");;

                            ?>
                        </td>
                        <td><?php print($rowaccount['lastupdated']);  ?></td>
                        <td><?php print($rowaccount['propfirm']);  ?>
                            <!--
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowaccount[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                            -->
                        </td>
                    </tr>



            <?php
        }
        //echo "tes";
    }

    ?>
    </tbody>
    </table>
    </div>


    <?php
    echo " <p>";
    listtradeacc1($objaccount,$tblname,$idcol,$menu);
    echo " <p>";
    listtradeacc2($objaccount,$tblname,$idcol,$menu);






}

function listtradeacc1($objaccount,$tblname,$idcol,$menu)
{
    //Challenges
    echo "<h2> LIVE ACCOUNTS</h2>";
    $uname = $_SESSION['username'];
    $query = "SELECT * FROM $tblname WHERE `status`<>'Completed' AND `targetbal`=0 AND `username`='$uname' ORDER BY `manager`,`phase`, `accountname`";
    $stmt = $objaccount->runQuery($query);
    $stmt->execute();

    ?>

                <?php
                $currmgr='';
                if($stmt->rowCount() > 0){
                while($rowaccount = $stmt->fetch(PDO::FETCH_ASSOC)){
                //echo "<h2>Managed by $currmgr </h2>";
                if ($currmgr=='' || $currmgr!==$rowaccount['manager']) {



                if($currmgr!==$rowaccount['manager']&&$currmgr!=='')
                {
                ?>
                    </tbody>
                    </table>
                    </div>
<?php
}

$currmgr = $rowaccount['manager'];
?>

<div class="table-responsive">
    <?php  echo " Managed by : $currmgr "; ?>
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>AccNo</th>
            <th>AccName</th>


            <th>Start Bal</th>
            <th>Today Equity</th>
            <th>Current Bal</th>
            <th>Equity</th>
            <th>P&L</th>

            <th>Days Left</th>
            <th>Last updated</th>
            <th>PropFirm</th>
        </tr>
        </thead>

        <tbody>
        <?php
        }

        ?>




        <tr>
            <td>
                <?php print($rowaccount[$idcol]);    ?>
            </td>

            <td>
                <!--<a href="index.php?menu=<?php echo $menu;?>&submenu=editaccount&edit_id=<?php print($rowaccount[$idcol]);?>">
                                <?php print($rowaccount['accountno']); ?></a>-->
                <?php print($rowaccount['accountno']); ?>
            </td>

            <td><?php print($rowaccount['accountname']);  ?></td>


            <td><?php print(number_format($rowaccount['startbal'],2,'.',','));  ?></td>
            <td><?php print(number_format($rowaccount['startdayequity'],2,'.',','));  ?></td>

            <td><?php print(number_format($rowaccount['currentbal'],2,'.',','));  ?></td>
            <td><?php print(number_format($rowaccount['equity'],2,'.',','));  ?></td>
            <td>
                <?php
                $pnl=$rowaccount['equity']-$rowaccount['startdayequity'];
                print(number_format($pnl,2,'.',','));
                ?>
            </td>

            <td>
                <?php

                //$remainbal=$rowaccount['startdate']-$rowaccount['currentbal'];
                $date1=date_create(date("Y-m-d"));
                $date2=date_create($rowaccount['enddate']);
                $diff=date_diff($date1,$date2);
                echo $diff->format("%R%a days");;

                ?>
            </td>
            <td><?php print($rowaccount['lastupdated']);  ?></td>
            <td><?php print($rowaccount['propfirm']);  ?>
                <!--
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowaccount[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                            -->
            </td>
        </tr>



        <?php
        }
        }
        ?>
    </tbody>
    </table>
    </div>
        <?php

}


function listtradeacc2($objaccount,$tblname,$idcol,$menu)
        {
        //Challenges
        echo "<h2> COMPLETED CHALLENGES</h2>";
        $uname = $_SESSION['username'];
        $query = "SELECT * FROM $tblname WHERE `status`='Completed' AND `username`='$uname' ORDER BY `manager`,`phase`, `accountname`";
        $stmt = $objaccount->runQuery($query);
        $stmt->execute();

        ?>

        <?php
        $currmgr='';
        if($stmt->rowCount() > 0){
        while($rowaccount = $stmt->fetch(PDO::FETCH_ASSOC)){
        //echo "<h2>Managed by $currmgr </h2>";
        if ($currmgr=='' || $currmgr!==$rowaccount['manager']) {



        if($currmgr!==$rowaccount['manager']&&$currmgr!=='')
        {
        ?>
        </tbody>
    </table>
</div>
<?php
}

$currmgr = $rowaccount['manager'];
?>

<div class="table-responsive">
    <?php  echo " Managed by : $currmgr "; ?>
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>AccNo</th>
            <th>AccName</th>

            <th>Ph</th>
            <th>Start Bal</th>

            <th>Current Bal</th>
            <th>Equity</th>

            <th>Remaining Target</th>
            <th>Last updated</th>
            <th>PropFirm</th>
        </tr>
        </thead>

        <tbody>
        <?php
        }

        ?>




        <tr>
            <td>
                <?php print($rowaccount[$idcol]);    ?>
            </td>

            <td>
                <!--<a href="index.php?menu=<?php echo $menu;?>&submenu=editaccount&edit_id=<?php print($rowaccount[$idcol]);?>">
                                <?php print($rowaccount['accountno']); ?></a>-->
                <?php print($rowaccount['accountno']); ?>
            </td>

            <td><?php print($rowaccount['accountname']);  ?></td>

            <td><?php print($rowaccount['phase']);  ?></td>
            <td><?php print(number_format($rowaccount['startbal'],2,'.',','));  ?></td>
            <td><?php print(number_format($rowaccount['currentbal'],2,'.',','));  ?></td>
            <td><?php print(number_format($rowaccount['equity'],2,'.',','));  ?></td>




            <td>
                <?php
                if($rowaccount['phase']<3)
                {
                    $remainbal=$rowaccount['targetbal']-$rowaccount['currentbal'];
                    print(number_format($remainbal,2,'.',','));
                }
                ?>
            </td>
            <td><?php print($rowaccount['lastupdated']);  ?></td>
            <td><?php print($rowaccount['propfirm']);  ?>
                <!--
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowaccount[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                            -->
            </td>
        </tr>



        <?php
        }
        }
        ?>
            </tbody>
            </table>
            </div>
        <?php

        }

function addaccount($objaccount,$tblname,$idcol,$menu)
{
    echo "test";
    accountform($objaccount,$tblname,$idcol,$menu);
}
function editaccount($objaccount,$tblname,$idcol,$menu)
{
    //echo "test";
    accountform($objaccount,$tblname,$idcol,$menu);
}

function accountform($objaccount,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objaccount->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowaccount = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowaccount = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowaccount[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="accountname" id="accountname" placeholder="First Name and Last Name" value="<?php print($rowaccount['accName']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Company *</label>
            <input  class="form-control" type="text" name="company" id="company" placeholder="Company Name" value="<?php print($rowaccount['accCompany']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Account Owner *</label>
            <input  class="form-control" type="text" name="owner" id="owner" placeholder="Owner Name" value="<?php print($rowaccount['accOwner']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Account Admin *</label>
            <input  class="form-control" type="text" name="admin" id="admin" placeholder="Admin Name" value="<?php print($rowaccount['accAdmin']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}

?>

