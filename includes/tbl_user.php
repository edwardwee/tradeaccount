<?php

function tbl_user_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="User Management";
    $objType="User";
    $tblname="tbl_user";
    $idcol="userid";
    require_once 'classes/class.tbl_user.php';
    $objUser = new User();
    processuser($objUser,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listuser':

                listuser($objUser,$tblname,$idcol,$menu);
                break;

            case 'adduser':
                adduser($objUser,$tblname,$idcol,$menu);
                break;
            case 'edituser':
                edituser($objUser,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listuser($objUser,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listuser($objUser,$tblname,$idcol,$menu);
    }

}
function processuser($objUser,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objUser->delete($id)){
                    $objUser->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['username'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['username']);
        $email  = strip_tags($_POST['email']);

        try{
            if($id !== ""){

                if($objUser->update($name, $email, $id)){

                    $objUser->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objUser->insert($name, $email)){
                    $objUser->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objUser->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listuser($objUser,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
<?php
    $query = "SELECT * FROM $tblname";
    $stmt = $objUser->runQuery($query);
    $stmt->execute();
?>
            <tbody>
<?php

    if($stmt->rowCount() > 0){
        while($rowUser = $stmt->fetch(PDO::FETCH_ASSOC)){
?>
                    <tr>
                        <td>
                            <?php print($rowUser[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=edituser&edit_id=<?php print($rowUser[$idcol]);?>">
                            <?php print($rowUser['username']); ?></a>
                        </td>

                        <td><?php print($rowUser['email']);  ?></td>

                        <td>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowUser[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                        </td>
                    </tr>

<?php
        }
    }
?>
            </tbody>
        </table>

    </div>
<?php
}

function adduser($objUser,$tblname,$idcol,$menu)
{
    echo "test";
    userform($objUser,$tblname,$idcol,$menu);
}
function edituser($objUser,$tblname,$idcol,$menu)
{
    //echo "test";
    userform($objUser,$tblname,$idcol,$menu);
}

function userform($objUser,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objUser->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowUser = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowUser = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowUser[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="username" id="username" placeholder="First Name and Last Name" value="<?php print($rowUser['username']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Email *</label>
            <input  class="form-control" type="text" name="email" id="email" placeholder="johndoel@gmail.com" value="<?php print($rowUser['email']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

<?php
}