<?php

function tbl_l3subactivity_mgt()
{
    $menu="";
    if(isset($_REQUEST['menu'])) {
        $menu = $_REQUEST['menu'];
    }

    $subpagetitle="l3subactivity Management";
    $objType="l3subactivity";
    $tblname="tbl_l3subactivity";
    $idcol="l3subactivityid";
    require_once 'classes/class.tbl_l3subactivity.php';
    $objl3subactivity = new l3subactivity();
    processl3subactivity($objl3subactivity,$menu);

    echo "<h1 style=\"margin-top: 10px\">$subpagetitle</h1>";

    //display status messages

    if(isset($_GET['updated'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!</strong> Updated with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['deleted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Deleted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['inserted'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType!<strong> Inserted with success.
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }else if(isset($_GET['error'])){
        echo "<div class=\"alert alert-info alert-dismissable fade show\" role=\"alert\">
                            <strong>$objType DB Error!<strong> Something went wrong with your action. Try again!
                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\"> &times; </span>
                              </button>
                            </div>";
    }


    if(isset($_REQUEST['submenu']))
    {
        $submenu=$_REQUEST['submenu'];

        switch ($submenu)
        {
            case 'listl3subactivity':

                listl3subactivity($objl3subactivity,$tblname,$idcol,$menu);
                break;

            case 'addl3subactivity':
                addl3subactivity($objl3subactivity,$tblname,$idcol,$menu);
                break;
            case 'editl3subactivity':
                editl3subactivity($objl3subactivity,$tblname,$idcol,$menu);
                break;
            default:
                //echo "HAHA";
                listl3subactivity($objl3subactivity,$tblname,$idcol,$menu);

        }
    }
    else
    {
        listl3subactivity($objl3subactivity,$tblname,$idcol,$menu);
    }

}
function processl3subactivity($objl3subactivity,$menu)
{

    if(isset($_GET['delete_id'])){

        $id = $_GET['delete_id'];
        try{
            if($id != null){
                if($objl3subactivity->delete($id)){
                    $objl3subactivity->redirect("index.php?menu=$menu&deleted");
                }
            }else{
                var_dump($id);
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
// POST
    //echo $_POST['l3subactivityname'];
    //echo $_POST['id'];
    if(isset($_REQUEST['btn_save'])){
        $id     = $_POST['id'];
        $name   = strip_tags($_POST['l3subactivityname']);
        $email  = strip_tags($_POST['email']);

        try{
            if($id !== ""){

                if($objl3subactivity->update($name, $email, $id)){

                    $objl3subactivity->redirect("index.php?menu=$menu&updated");
                }
            }else{

                if($objl3subactivity->insert($name, $email)){
                    $objl3subactivity->redirect("index.php?menu=$menu&inserted");
                }else{
                    $objl3subactivity->redirect("index.php?menu=$menu&error");
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
function listl3subactivity($objl3subactivity,$tblname,$idcol,$menu)
{
    //echo "test";
    ?>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <?php
            $query = "SELECT * FROM $tblname";
            $stmt = $objl3subactivity->runQuery($query);
            $stmt->execute();
            ?>
            <tbody>
            <?php

            if($stmt->rowCount() > 0){
                while($rowl3subactivity = $stmt->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <tr>
                        <td>
                            <?php print($rowl3subactivity[$idcol]);    ?>
                        </td>

                        <td>
                            <a href="index.php?menu=<?php echo $menu;?>&submenu=editl3subactivity&edit_id=<?php print($rowl3subactivity[$idcol]);?>">
                                <?php print($rowl3subactivity['l3subactivityname']); ?></a>
                        </td>

                        <td><?php print($rowl3subactivity['email']);  ?></td>

                        <td>
                            <a class="confirmation" href="index.php?menu=<?php echo $menu;?>&delete_id=<?php echo $rowl3subactivity[$idcol]; ?>">
                                <span data-feather="trash"></span>
                            </a>
                        </td>
                    </tr>

                    <?php
                }
            }
            ?>
            </tbody>
        </table>

    </div>
    <?php
}

function addl3subactivity($objl3subactivity,$tblname,$idcol,$menu)
{
    echo "test";
    l3subactivityform($objl3subactivity,$tblname,$idcol,$menu);
}
function editl3subactivity($objl3subactivity,$tblname,$idcol,$menu)
{
    //echo "test";
    l3subactivityform($objl3subactivity,$tblname,$idcol,$menu);
}

function l3subactivityform($objl3subactivity,$tblname,$idcol,$menu)
{

    // GET
    if(isset($_GET['edit_id'])){
        $id = $_GET['edit_id'];
        $stmt = $objl3subactivity->runQuery("SELECT * FROM $tblname WHERE $idcol=:$idcol");
        $stmt->execute(array(":$idcol" => $id));
        $rowl3subactivity = $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $id = null;
        $rowl3subactivity = null;
    }




    ?>
    <h2 style="margin-top: 10px">Add / Edit </h2>
    <p>Required fields are in (*)</p>
    <form  method="post">
        <input type="hidden" name="menu" value="<?php echo $menu;?>">
        <div class="form-group">

            <label for="id">ID</label>
            <input class="form-control" type="text" name="id" id="id" value="<?php print($rowl3subactivity[$idcol]); ?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Name *</label>
            <input  class="form-control" type="text" name="l3subactivityname" id="l3subactivityname" placeholder="First Name and Last Name" value="<?php print($rowl3subactivity['l3subactivityname']); ?>" required maxlength="100">
        </div>
        <div class="form-group">
            <label for="email">Email *</label>
            <input  class="form-control" type="text" name="email" id="email" placeholder="johndoel@gmail.com" value="<?php print($rowl3subactivity['email']); ?>" required maxlength="100">
        </div>
        <input class="btn btn-primary mb-2" type="submit" name="btn_save" value="Save">
    </form>

    <?php
}