<?php
// Start the session
session_save_path('./tmp');
ob_start();
@session_start();

// Show PHP errors
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);


require_once 'includes/config.php';
require_once 'includes/login.php';
$logout=0;
if(isset($_REQUEST['menu']))
{
    if($_REQUEST['menu']=='logout')
    {
        session_unset();
        $logout=1;
    }
}
$rlogin="";

if (isset($_REQUEST['loginflag'])) {

    $rlogin = login();
}

$menu="";
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Head metas, css, and title -->
        <?php require_once 'includes/head.php'; ?>
        <?php
        if($menu=='tradeacc')
            {
                echo "<meta http-equiv=\"refresh\" content=\"5\">";


            }
        ?>
        <!--Credits to -->
        <!--
            Initial Designed Versions Inspired by Joan, Marvin Kong
        -->
    </head>
    <body>

        <!-- Header banner -->
        <?php require_once 'includes/header.php'; ?>
        <div class="container-fluid">
            <div class="row">
                <!-- Sidebar menu -->
                <?php require_once 'includes/sidebar.php'; ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">




                    <?php
                    echo $rlogin;
                    if($logout==1)
                    {
                        echo "Logout Successful";
                    }
                    if(!isset($_SESSION['loggedin']))
                    {
                        loginfrm();
                    }
                    else {
                        //echo $_SESSION['username'];
                        require_once 'includes/content.php';
                    }
                     ?>



                </main>
            </div>
        </div>
        <!-- Footer scripts, and functions -->
        <?php require_once 'includes/footer.php'; ?>

        <!-- Custom scripts -->
        <script>
            // JQuery confirmation
            $('.confirmation').on('click', function () {
                return confirm('Are you sure you want do delete this user?');
            });
        </script>
    </body>
</html>
