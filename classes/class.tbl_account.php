<?php

global $dbpath;
require_once $dbpath;

class account {
    private $conn;
    private $tblname;


    // Constructor
    public function __construct(){
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
        $this->tblname="tbl_account";

    }


    // Execute queries SQL
    public function runQuery($sql){
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    // Insert
    public function insert($name,$company,$owner,$admin){
        try{
            $stmt = $this->conn->prepare("INSERT INTO ".$this->tblname." (accName, accCompany,accOwner,accAdmin) VALUES(:name, :company,:owner,:admin)");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":company", $company);
            $stmt->bindparam(":owner", $owner);
            $stmt->bindparam(":admin", $admin);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Update
    public function update($name,$company,$owner,$admin, $id){
        try{
            echo "testupdate";
            $stmt = $this->conn->prepare("UPDATE ".$this->tblname." SET accName = :name, accOwner = :owner,accCompany=:company,accAdmin=:admin WHERE accID = :id");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":company", $company);
            $stmt->bindparam(":owner", $owner);
            $stmt->bindparam(":admin", $admin);
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Delete
    public function delete($id){
        try{
            $stmt = $this->conn->prepare("DELETE FROM ".$this->tblname." WHERE accID = :id");
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    // Redirect URL method
    public function redirect($url){
        header("Location: $url");
    }
}
?>
