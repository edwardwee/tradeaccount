<?php
global $dbpath;
require_once $dbpath;

class l1proj {
    private $conn;
    private $tblname;


    // Constructor
    public function __construct(){
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
        $this->tblname="tbl_l1proj";
        $this->idcol="projID";

    }


    // Execute queries SQL
    public function runQuery($sql){
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    // Insert
    public function insert($accID, $projName,$projRef,$projStatus,$projManager,$projManagerTeam,$projLocation,$projDetailLocation,$projTargetStart,$projTargetEnd,$projScheduleStart,$projScheduleEnd){
        try{
            $curdate=date("Y-m-d");
            $stmt = $this->conn->prepare("INSERT INTO ".$this->tblname." (`accID`,`projName`, `projRef`, `projStatus`, `projManager`, `projManagerTeam`, `projLocation`, `projDetailLocation`, `projTargetStart`, `projTargetEnd`, `projScheduleStart`, `projScheduleEnd`, `CreatedOn`, `ModifiedOn`) VALUES
            (:accID,:projName, :projRef, :projStatus, :projManager, :projManagerTeam, :projLocation, :projDetailLocation, :projTargetStart, :projTargetEnd, 
            :projScheduleStart, :projScheduleEnd, '$curdate', '$curdate')");
            $stmt->bindparam(":accID", $accID);
            $stmt->bindparam(":projName", $projName);
            $stmt->bindparam(":projRef", $projRef);
            $stmt->bindparam(":projStatus", $projStatus);
            $stmt->bindparam(":projManager", $projManager);
            $stmt->bindparam(":projManagerTeam", $projManagerTeam);
            $stmt->bindparam(":projLocation", $projLocation);
            $stmt->bindparam(":projDetailLocation", $projDetailLocation);
            $stmt->bindparam(":projTargetStart", $projTargetStart);
            $stmt->bindparam(":projTargetEnd", $projTargetEnd);
            $stmt->bindparam(":projScheduleStart", $projScheduleStart);
            $stmt->bindparam(":projScheduleEnd", $projScheduleEnd);

            $stmt->execute();
  //          echo $stmt;
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Update
    public function update($accID, $projName,$projRef,$projStatus,$projManager,$projManagerTeam,$projLocation,$projDetailLocation,$projTargetStart,$projTargetEnd,$projScheduleStart,$projScheduleEnd,$projActStart,$projActEnd, $id){
        try{
            //echo "testupdate";
            $tblname=$this->tblname;
            $idcol=$this->idcol;
            $curdate=date("Y-m-d");
            $stmt = $this->conn->prepare("UPDATE `$tblname` SET 
                
                `accID`=:accID,
                `projName`=:projName,
                 `projRef`=:projRef,
                 `projStatus`=:projStatus,
                 `projManager`=:projManager,
                 `projManagerTeam`=:projManagerTeam,
                 `projLocation`=:projLocation,
                 `projDetailLocation`=:projDetailLocation,
                 `projTargetStart`=:projTargetStart,
                 `projTargetEnd`=:projTargetEnd,
                 `projScheduleStart`=:projScheduleStart,
                 `projScheduleEnd`=:projScheduleEnd,
                 `projActStart`=:projActStart,
                 `projActEnd`=:projActEnd,
                 `ModifiedOn`='$curdate' 
                
                WHERE `$idcol` = :id");

            $stmt->bindparam(":accID", $accID);
            $stmt->bindparam(":projName", $projName);
            $stmt->bindparam(":projRef", $projRef);
            $stmt->bindparam(":projStatus", $projStatus);
            $stmt->bindparam(":projManager", $projManager);
            $stmt->bindparam(":projManagerTeam", $projManagerTeam);
            $stmt->bindparam(":projLocation", $projLocation);
            $stmt->bindparam(":projDetailLocation", $projDetailLocation);
            $stmt->bindparam(":projTargetStart", $projTargetStart);
            $stmt->bindparam(":projTargetEnd", $projTargetEnd);
            $stmt->bindparam(":projScheduleStart", $projScheduleStart);
            $stmt->bindparam(":projScheduleEnd", $projScheduleEnd);
            $stmt->bindparam(":projActStart", $projActStart);
            $stmt->bindparam(":projActEnd", $projActEnd);
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Delete
    public function delete($id){
        try{
            $tblname=$this->tblname;
            $idcol=$this->idcol;
            $stmt = $this->conn->prepare("DELETE FROM `$tblname` WHERE `$idcol` = :id");
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    // Redirect URL method
    public function redirect($url){
        header("Location: $url");
    }
}
?>
