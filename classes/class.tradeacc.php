<?php

global $dbpath;
require_once $dbpath;

class account {
    private $conn;
    private $tblname;


    // Constructor
    public function __construct(){
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
        $this->tblname="tbl_account";

    }


    // Execute queries SQL
    public function runQuery($sql){
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    // Insert
    public function insert($id,$accno,$accholder,$manager, $propfirm, $phase, $description, $status,$startbal,$currentbal,$targetbal,$startdate,$enddate){
        try{
            $stmt = $this->conn->prepare("INSERT INTO ".$this->tblname." (accountno, accholder,manager,propfirm,phase,description,status,startbal,currentbal,targetbal,startdate,enddate) VALUES(:accno, :accholder,:manager,:propfirm,:phase,:description,:status,:startbal,:currentbal,:targetbal,:startdate,:enddate)");
            $stmt->bindparam(":accountno", $accno);
            $stmt->bindparam(":accountname", $accholder);
            $stmt->bindparam(":manager", $manager);
            $stmt->bindparam(":propfirm", $propfirm);
            $stmt->bindparam(":phase", $phase);
            $stmt->bindparam(":description", $description);
            $stmt->bindparam(":status", $status);
            $stmt->bindparam(":startbal", $startbal);
            $stmt->bindparam(":currentbal", $currentbal);
            $stmt->bindparam(":targetbal", $targetbal);
            $stmt->bindparam(":startdate", $startdate);
            $stmt->bindparam(":enddate", $enddate);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Update
    public function update($id,$accno,$accholder,$manager, $propfirm, $phase, $description, $status,$startbal,$currentbal,$targetbal,$startdate,$enddate){
        try{
            echo "testupdate";
            $stmt = $this->conn->prepare("UPDATE ".$this->tblname." SET accountno=:accountno, accholder=:accholder,manager=:manager,propfirm=:propfirm,phase=:phase,description=:description,status=:status,startbal=:startbal,currentbal=:currentbal,targetbal=:targetbal,startdate=:startdate,enddate=:enddate WHERE accID = :id");
            $stmt->bindparam(":accountno", $accno);
            $stmt->bindparam(":accountname", $accholder);
            $stmt->bindparam(":manager", $manager);
            $stmt->bindparam(":propfirm", $propfirm);
            $stmt->bindparam(":phase", $phase);
            $stmt->bindparam(":description", $description);
            $stmt->bindparam(":status", $status);
            $stmt->bindparam(":startbal", $startbal);
            $stmt->bindparam(":currentbal", $currentbal);
            $stmt->bindparam(":targetbal", $targetbal);
            $stmt->bindparam(":startdate", $startdate);
            $stmt->bindparam(":enddate", $enddate);
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Delete
    public function delete($id){
        try{
            $stmt = $this->conn->prepare("DELETE FROM ".$this->tblname." WHERE id = :id");
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    // Redirect URL method
    public function redirect($url){
        header("Location: $url");
    }
}
?>
