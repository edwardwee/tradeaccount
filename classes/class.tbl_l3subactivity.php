<?php

global $dbpath;
require_once $dbpath;

class l3subactivity {
    private $conn;
    private $tblname;


    // Constructor
    public function __construct(){
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
        $this->tblname="tbl_l3subactivity";

    }


    // Execute queries SQL
    public function runQuery($sql){
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    // Insert
    public function insert($name, $email){
        try{
            $stmt = $this->conn->prepare("INSERT INTO ".$this->tblname." (l3subactivityname, email) VALUES(:name, :email)");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":email", $email);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Update
    public function update($name, $email, $id){
        try{
            echo "testupdate";
            $stmt = $this->conn->prepare("UPDATE ".$this->tblname." SET l3subactivityname = :name, email = :email WHERE l3subactivityid = :id");
            $stmt->bindparam(":name", $name);
            $stmt->bindparam(":email", $email);
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    // Delete
    public function delete($id){
        try{
            $stmt = $this->conn->prepare("DELETE FROM ".$this->tblname." WHERE l3subactivityid = :id");
            $stmt->bindparam(":id", $id);
            $stmt->execute();
            return $stmt;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    // Redirect URL method
    public function redirect($url){
        header("Location: $url");
    }
}
?>
